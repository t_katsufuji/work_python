
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import selenium.webdriver.chrome.service as service

service = service.Service('c:\selenium\chromedriver')
service.start()
capabilities = {'chrome.binary': '/path/to/custom/chrome'}
driver = webdriver.Remote(service.service_url, capabilities)
driver.get('http://www.google.com/xhtml');
time.sleep(5) # Let the user actually see something!

# Input serch keyword
element = driver.find_element_by_name("q")
element.send_keys("SerchKeyword")
element.send_keys(Keys.RETURN)


time.sleep(3) # Let the user actually see something!

driver.quit()
