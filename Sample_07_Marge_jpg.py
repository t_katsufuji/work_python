# coding:utf-8

# pillowのimport
from PIL import Image as img

if __name__ == '__main__':

	try:
		# 対象のイメージを取得
		a_jpg = img.open("C:\Selenium\screenshot\screen_Top1.png") 
		b_jpg = img.open("C:\Selenium\screenshot\screen_Top2.png") 

		# imageのサイズを出力
		print(a_jpg.filename + "：" + str(a_jpg.size))
		print(b_jpg.filename + "：" + str(b_jpg.size))

		# imageの高さを取得
		height_a = a_jpg.height
		height_b = b_jpg.height

		# imageの幅を取得
		width_canvas = a_jpg.width

		# 結合した時のサイズでキャンバスを作成
		height_canvas = height_a + height_b
		width_canvas = width_canvas
		canvas = img.new("RGB",(width_canvas, height_canvas),(255,255,255))
		
		# キャンバスに画像を貼り付ける
		canvas.paste(a_jpg,(0,0))
		canvas.paste(b_jpg,(0,868))
		
		# 画像ファイルとしてを保存
		canvas.save('C:\Selenium\screenshot\marge.jpg', 'JPEG', quality=100, optmize=True)
		
	except:
		import traceback
		traceback.print_exc()

	else:
		print('正常終了')

	finally:
		pass #driver.quit()
