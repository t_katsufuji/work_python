# coding:utf-8

import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import selenium.webdriver.chrome.service as service

service = service.Service('c:\selenium\chromedriver')
service.start()

capabilities = {'chrome.binary': '/path/to/custom/chrome'}
driver = webdriver.Remote(service.service_url, capabilities)
print(driver.name)

driver.get('http://v3.apamanshop.com:8000/tokyo/');
time.sleep(2) # Let the user actually see something!

try:

	# Select checkBox
	element = driver.find_element_by_id("13104")
	check = element.find_element_by_name("target")
	check.click()

	# 画面上に表示されていないオブジェクトに対する操作はエラーとなる
	#element = driver.find_element_by_id("13228")
	#check = element.find_element_by_name("target")
	#check.click()

	time.sleep(1) # Let the user actually see something!

	# Save screenshot
	driver.save_screenshot('C:\Selenium\screenshot\screenshot.png')

	time.sleep(2) # Let the user actually see something!

except:
	pass

else:
	pass

finally:
	driver.quit()
