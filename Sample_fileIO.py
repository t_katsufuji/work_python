# coding:utf-8

# file open
f = open('C:\Selenium\Sample.py', 'r')

# 一行読み出し
f.readline()
f.readline()
f.readline()

# file close
f.close()


# file,directoryの存在チェック
import os

filepath = 'C:\Selenium\SampleD.txt'

# ファイルパスの存在チェック
print (os.path.exists(filepath))
if os.path.exists(filepath):
	print("指定されたファイルパスは既に存在しています。")

if not os.path.exists(filepath):
	print("指定されたファイルパスは既に存在していません。")

# ファイルのチェック
print (os.path.isfile(filepath))

# ディレクトリのチェック
print (os.path.isdir(filepath))

# ファイルパスが存在しなかった場合にディレクトリの作成
if not os.path.exists(filepath):
	#os.mkdir(filepath)
	os.makedirs(filepath)
	print ("ディレクトリを作成")
