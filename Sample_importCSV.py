# coding:utf-8

import csv
import codecs

# file open
with open("c:/selenium/readfile/import_Sample.csv", encoding='utf-8') as csvfile:
	reader = csv.reader(csvfile)

	# get header and next row
	header = next(reader)
	print(header)
	
	
	# output data row
	for row in reader:
		print(reader.line_num)
		print(row)

		# output data column
		for col in row:
			print(col)

# file close
csvfile.close()