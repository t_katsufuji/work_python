# coding:utf-8

import datetime

# 日時を取得
now = datetime.datetime.today()
print(now)

# 日時を加工して表示①
a = now.strftime("%m-%d-%y. %d %b %Y is a %A on the %d day of %B.")
print(a)

# 日時を加工して表示②
a = now.strftime("%Y%m%d_%H%M%S")
print(a)

