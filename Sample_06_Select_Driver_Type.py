import sys
import time
from selenium import webdriver

browser_type = input("Input browser type.(1:Chrome 2:Firefox) : ")

if browser_type == "1":
	driver = webdriver.Chrome('C:\Selenium\chromedriver.exe')  # Optional argument, if not specified will search path.
	keyword = "chromedriver"

elif browser_type == "2":
	driver = webdriver.Firefox()  # Optional argument, if not specified will search path.
	keyword = "Geckodriver"

else:
	print("end")
	sys.exit()

time.sleep(5)
driver.get('http://www.google.com/xhtml');
search_box = driver.find_element_by_name('q')
search_box.send_keys(keyword)
search_box.submit()
time.sleep(3)
driver.quit()