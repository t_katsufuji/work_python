# coding:utf-8

import sys
import time
from selenium import webdriver
#from selenium.webdriver.common.keys import Keys
#from selenium.webdriver.common.by import By

import selenium.webdriver.chrome.service as service

service = service.Service('c:\selenium\chromedriver')
service.start()

capabilities = {'chrome.binary': '/path/to/custom/chrome'}
driver = webdriver.Remote(service.service_url, capabilities)
driver.set_page_load_timeout(30)
print(driver.name)

driver.get('http://v3.apamanshop.com:8000/tokyo/');

try:

	# Chenge brawser window size
	driver.set_window_position(0,0)
	driver.set_window_size(1100,1000)
	#driver.maximize_window()
	time.sleep(3)

	# Get brawser window info
	height = driver.execute_script("return window.innerHeight;")

	# Get Copyright object position
	element = driver.find_element_by_id("siteCopyright")
	copyright_pos = element.location['y']

	# Save screenshot
	folder_name = "C:\\Selenium\\screenshot\\"

	i = 1
	while i <= (copyright_pos // height) + 1:
		#Save screen
		folder_name = "C:\\Selenium\\screenshot\\"
		file_name = "screen_" + str(i) + ".png"
		driver.save_screenshot(folder_name + file_name)
		# Scroll
		script = "window.scrollTo(0, " + str(height*i) + ");"
		driver.execute_script(script)
		i += 1

#	time.sleep(3)
#	driver.save_screenshot('C:\Selenium\screenshot\screen_1.png')
#	time.sleep(2)
#	driver.execute_script("window.scrollTo(0, 868);")
#	driver.save_screenshot('C:\Selenium\screenshot\screen_2.png')
#	time.sleep(2)
#	driver.execute_script("window.scrollTo(0, 1736);")
#	driver.save_screenshot('C:\Selenium\screenshot\screen_3.png')
#	time.sleep(2)
#	driver.execute_script("window.scrollTo(0, 2604);")
#	driver.save_screenshot('C:\Selenium\screenshot\screen_4.png')
#	time.sleep(3)


except:
	import traceback
	traceback.print_exc()

else:
	print('正常終了')

finally:
	driver.quit()
